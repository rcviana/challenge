# README #

### Programs required to run the project ###

* JDK 11 
* [Maven](https://maven.apache.org/install.html)
* [Docker for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/) with Kubernetes enabled
* [Skaffold](https://skaffold.dev/docs/install/)

### How to build and deploy the project ###

1. On a terminal, navigate to the ``api`` folder and run: 
> ``mvn clean install``
2. Still on the ``api`` folder, run: 
> ``docker build -t challenge-api .``
3. In order to be able to call the API from the outside of the local cluster, with Postman for example, 
we need to install [ingress-nginx](https://kubernetes.github.io/ingress-nginx/) services by running: 
> ``kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/cloud/deploy.yaml``
4. Navigate to the root path where the ``skaffold.yaml`` file is located and run: 
> ``skaffold dev``
5. The ingress is configured to expose the API through the host ``https://challenge.com``, so we need to configure
our local ``hosts`` file to make sure that anytime we try to call this host it is redirected to our local machine:
    * on Windows the ``hosts`` file is located on: ``C:\Windows\System32\Drivers\etc\hosts``
    * we need to open this file with a text editor in administrator mode
    * and add ``127.0.0.1 challenge.com`` at the end of the file

On a different terminal, if we run: ``kubectl get deployments``, we should see these two deployments:
 
* challenge-api
* challenge-api-mongo-db

If you need to uninstall them, navigate to the ``k8s`` folder and run: 
> ``kubectl delete -f .``

### Examples ###

1. Creating site:
![1_creating_site](./examples/1_creating_site.png)

1. Creating question:
![2_creating_question](./examples/2_creating_question.png)

1. Creating answers:
![3_creating_question_answers](./examples/3_creating_question_answers.png)

1. Creating matrix answers:
![4_creating_matrix_question_answers](./examples/4_creating_matrix_question_answers.png)

1. User fetching matrix question:
![4_creating_matrix_question_answers](./examples/5_user_fetching_matrix_question.png)

1. User answering matrix question:
![6_user_answering_matrix_question](./examples/6_user_answering_matrix_question.png)

### Why MongoDB? ###

*Document databases are extremely flexible, allowing variations in the structure of documents and allowing storage 
of documents that are partially complete.
From its founding, MongoDB was built on a scale-out architecture, a structure that allows many small machines 
to work together to create systems that are fast and handle huge amounts of data.*

Source: https://www.mongodb.com/why-use-mongodb

### Security ###

Considering the CIA triad:
  
* Confidentiality: failure of confidentiality occurs if someone can obtain and view the data
* Availability: failure of availability occurs if the data cannot be accessed by the end user
* Integrity: failure of integrity occurs if someone modifies the data being stored or when it is in transit

Availability and Integrity could be more easily addressed by: 

* increasing the number of replicas of the API service
* using trusted certificate instead of the self-signed certificate provided by ingress-nginx

To improve Confidentiality though, we would need to require some kind of authentication, where only permitted 
users could use the API, also authorization so only users with the admin role could create, update or delete 
sites, questions, etc.

### Scaling ###

##### Kubernetes #####

For containerized micro-services architecture, Kubernetes may be very helpful by providing the basis for scaling 
application without huge effort.   

*Containers are a good way to bundle and run your applications. In a production environment, you need to manage 
the containers that run the applications and ensure that there is no downtime. For example, if a container 
goes down, another container needs to start.* 

*That's how Kubernetes comes to the rescue! Kubernetes provides you with a framework to run distributed 
systems resiliently. It takes care of scaling and failover for your application, provides deployment patterns, 
and more. For example, Kubernetes can easily manage a canary deployment for your system.*

Source: https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/#why-you-need-kubernetes-and-what-can-it-do

##### Reactive Stack #####

The use of a non-blocking framework like Spring WebFlux could also help to handle concurrency with a small number
of threads and scale with fewer hardware resources.

Source: https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html

### TODOs ###
* K8s Persistent Volume to avoid losing data when MongoDB's pod is restarted
* Integrate Helm Charts to support different environments 
* Improve API response format, adding more details when Bad Requests happen