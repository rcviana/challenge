package com.challenge.demo.validators;

import com.challenge.demo.dtos.QuestionAnswerDTO;
import com.challenge.demo.dtos.QuestionAnswerDataDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TriviaQuestionValidatorTest {

    @Autowired
    private TriviaQuestionValidator triviaQuestionValidator;

    @Test(expected = IllegalArgumentException.class)
    public void whenValidateTriviaQuestion_givenLessThanTwoAnswers_thenThrowException() {
        triviaQuestionValidator.validate(mockAnswerDataWithNAnswers(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenValidateTriviaQuestion_givenMoreThanFourAnswers_thenThrowException() {
        triviaQuestionValidator.validate(mockAnswerDataWithNAnswers(5));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenValidateTriviaQuestion_givenMoreThanOneCorrectAnswer_thenThrowException() {
        triviaQuestionValidator.validate(mockAnswerDataWithNCorrectAnswers());
    }

    private QuestionAnswerDataDTO mockAnswerDataWithNAnswers(int numberOfAnswers) {
        List<QuestionAnswerDTO> questionAnswerDTOS = new ArrayList<>();
        for (int i = 0; i < numberOfAnswers; i++) {
            QuestionAnswerDTO questionAnswerDTO = new QuestionAnswerDTO();
            questionAnswerDTOS.add(questionAnswerDTO);
        }

        QuestionAnswerDataDTO answerDataDTO = new QuestionAnswerDataDTO();
        answerDataDTO.setAnswers(questionAnswerDTOS);

        return answerDataDTO;
    }

    private QuestionAnswerDataDTO mockAnswerDataWithNCorrectAnswers() {
        QuestionAnswerDataDTO questionAnswerDataDTO = mockAnswerDataWithNAnswers(2);
        questionAnswerDataDTO.getAnswers().forEach(answer -> answer.setIsCorrectAnswer(true));
        return questionAnswerDataDTO;
    }
}
