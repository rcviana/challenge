package com.challenge.demo.dtos;

import com.challenge.demo.entities.Question;
import com.challenge.demo.entities.UserAnswer;

import java.util.List;
import java.util.UUID;

public class UserAnswerDTO {
    private String userUUID;
    private String siteUUID;
    private String column;
    private List<String> answers;
    private String questionId;

    public static UserAnswerDTO build(final UserAnswer userAnswer) {
        final UserAnswerDTO newUserAnswerDTO = new UserAnswerDTO();
        newUserAnswerDTO.setAnswers(userAnswer.getAnswers());
        newUserAnswerDTO.setColumn(userAnswer.getColumn());
        newUserAnswerDTO.setQuestionId(userAnswer.getQuestion().getQuestionId());
        newUserAnswerDTO.setSiteUUID(userAnswer.getSiteUUID().toString());
        newUserAnswerDTO.setUserUUID(userAnswer.getUserUUID().toString());
        return newUserAnswerDTO;
    }

    public static UserAnswer createUserAnswer(final UserAnswerDTO incomingUserAnswer, final Question question) {
        final UserAnswer newUserAnswer = new UserAnswer();
        newUserAnswer.setAnswers(incomingUserAnswer.getAnswers());
        newUserAnswer.setColumn(incomingUserAnswer.getColumn());
        newUserAnswer.setQuestion(question);
        newUserAnswer.setSiteUUID(UUID.fromString(incomingUserAnswer.getSiteUUID()));
        newUserAnswer.setUserUUID(UUID.fromString(incomingUserAnswer.getUserUUID()));
        return newUserAnswer;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getSiteUUID() {
        return siteUUID;
    }

    public void setSiteUUID(String siteUUID) {
        this.siteUUID = siteUUID;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    @Override
    public String toString() {
        return "UserAnswerDTO{" +
                "userUUID='" + userUUID + '\'' +
                ", siteUUID='" + siteUUID + '\'' +
                ", column='" + column + '\'' +
                ", answers='" + answers + '\'' +
                ", questionId='" + questionId + '\'' +
                '}';
    }
}
