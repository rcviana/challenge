package com.challenge.demo.dtos;

import java.util.List;

public class QuestionAnswerDataDTO {

    private String type;
    private List<String> columns;
    private List<QuestionAnswerDTO> answers;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<QuestionAnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<QuestionAnswerDTO> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "QuestionAnswerDataDTO{" +
                "type='" + type + '\'' +
                ", columns=" + columns +
                ", answers=" + answers +
                '}';
    }
}
