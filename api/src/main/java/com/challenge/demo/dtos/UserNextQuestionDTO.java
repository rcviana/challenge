package com.challenge.demo.dtos;

import java.util.List;

public class UserNextQuestionDTO {

    private String userUUID;
    private String siteUUID;
    private String questionId;
    private List<String> columns;
    private List<String> answers;

    public UserNextQuestionDTO(String userUUID, String siteUUID, String questionId, List<String> columns,
                               List<String> answers) {
        this.userUUID = userUUID;
        this.siteUUID = siteUUID;
        this.questionId = questionId;
        this.columns = columns;
        this.answers = answers;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getSiteUUID() {
        return siteUUID;
    }

    public void setSiteUUID(String siteUUID) {
        this.siteUUID = siteUUID;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "UserNextQuestionDTO{" +
                "userUUID='" + userUUID + '\'' +
                ", siteUUID='" + siteUUID + '\'' +
                ", columns=" + columns +
                ", answers=" + answers +
                '}';
    }
}
