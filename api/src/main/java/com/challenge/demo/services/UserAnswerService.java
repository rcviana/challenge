package com.challenge.demo.services;

import com.challenge.demo.dtos.QuestionAnswerDTO;
import com.challenge.demo.dtos.QuestionAnswerDataDTO;
import com.challenge.demo.dtos.UserNextQuestionDTO;
import com.challenge.demo.entities.Question;
import com.challenge.demo.entities.Site;
import com.challenge.demo.entities.UserAnswer;
import com.challenge.demo.repositories.QuestionRepository;
import com.challenge.demo.repositories.SiteRepository;
import com.challenge.demo.repositories.UserAnswerRepository;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class UserAnswerService {

    private Logger logger = LoggerFactory.getLogger(UserAnswerService.class);

    private final SiteRepository siteRepository;
    private final QuestionRepository questionRepository;
    private final UserAnswerRepository userAnswerRepository;

    public UserAnswerService(final SiteRepository siteRepository, final QuestionRepository questionRepository,
                      UserAnswerRepository userAnswerRepository) {
        this.siteRepository = siteRepository;
        this.questionRepository = questionRepository;
        this.userAnswerRepository = userAnswerRepository;
    }

    public UserNextQuestionDTO getNextQuestion(UUID userUUID, UUID siteUUID) {

        Site site = siteRepository.findBySiteUUID(siteUUID);
        if (Optional.ofNullable(site).isEmpty()) {
            return null;
        }

        List<Question> questions = questionRepository.findBySiteSiteId(site.getSiteId());
        if (questions == null || questions.isEmpty()) {
            return null;
        }

        List<UserAnswer> userAnswers = userAnswerRepository.findByUserUUIDAndSiteUUID(userUUID, siteUUID);
        if (userAnswers == null || userAnswers.isEmpty()) {
            return extractQuestionData(userUUID, siteUUID, questions.get(0));
        }

        List<Question> answeredQuestions = extractUserAnsweredQuestions(userAnswers);

        questions.removeAll(answeredQuestions);
        if (!questions.isEmpty()) {
            return extractQuestionData(userUUID, siteUUID, questions.get(0));
        }

        userAnswerRepository.removeAllByUserUUIDAndSiteUUID(userUUID, siteUUID);

        return getNextQuestion(userUUID, siteUUID);
    }

    private UserNextQuestionDTO extractQuestionData(UUID userUUID, UUID siteUUID, Question question) {

        if (question.getAnswersData() == null) {
            throw new IllegalStateException(
                    String.format("The question identified by the id '%s' does not have the proper answer information!",
                            question.getQuestionId()));
        }

        QuestionAnswerDataDTO answerDataDTO = new Gson().fromJson(question.getAnswersData().toString(),
                QuestionAnswerDataDTO.class);

        return new UserNextQuestionDTO(userUUID.toString(), siteUUID.toString(), question.getQuestionId(),
                answerDataDTO.getColumns(), extractQuestionAnswers(answerDataDTO.getAnswers()));
    }

    private List<String> extractQuestionAnswers(List<QuestionAnswerDTO> questionAnswerDTOS) {
        return questionAnswerDTOS.stream()
                .map(QuestionAnswerDTO::getAnswer)
                .collect(Collectors.toList());
    }

    private List<Question> extractUserAnsweredQuestions(List<UserAnswer> userAnswers) {
        return userAnswers.stream()
                .map(UserAnswer::getQuestion)
                .collect(Collectors.toList());
    }
}
