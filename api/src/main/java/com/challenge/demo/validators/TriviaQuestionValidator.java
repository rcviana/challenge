package com.challenge.demo.validators;

import com.challenge.demo.dtos.QuestionAnswerDTO;
import com.challenge.demo.dtos.QuestionAnswerDataDTO;
import org.springframework.stereotype.Component;

@Component
public class TriviaQuestionValidator {

    void validate(QuestionAnswerDataDTO answerDataDTO) {
        int answersSize = answerDataDTO.getAnswers().size();
        if (answersSize < 2 || answersSize > 4) {
            throw new IllegalArgumentException("This type of question requires at least 2 and at most 4 answers!");
        }

        int correctAnswerCount = 0;
        for (QuestionAnswerDTO answer : answerDataDTO.getAnswers()) {
            if (answer.getIsCorrectAnswer()) {
                correctAnswerCount++;
            }
        }

        if (correctAnswerCount != 1) {
            throw new IllegalArgumentException("This type of question requires one correct answer!");
        }
    }
}
