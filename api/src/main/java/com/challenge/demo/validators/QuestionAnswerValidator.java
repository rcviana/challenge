package com.challenge.demo.validators;

import com.challenge.demo.dtos.QuestionAnswerDataDTO;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

@Component
public class QuestionAnswerValidator {

    private enum QuestionTypeEnum {
        TRIVIA,
        POLL,
        CHECKBOX,
        MATRIX
    }

    final private CheckboxQuestionValidator checkboxAnswerValidator;
    final private PollQuestionValidator pollAnswerValidator;
    final private TriviaQuestionValidator triviaAnswerValidator;

    public QuestionAnswerValidator(CheckboxQuestionValidator checkboxAnswerValidator,
                                   PollQuestionValidator pollAnswerValidator,
                                   TriviaQuestionValidator triviaAnswerValidator) {

        this.checkboxAnswerValidator = checkboxAnswerValidator;
        this.pollAnswerValidator = pollAnswerValidator;
        this.triviaAnswerValidator = triviaAnswerValidator;
    }

    public void validate(Object answerData) {
        if (answerData == null || answerData.toString().isEmpty()) {
            throw new IllegalArgumentException("The answer information is required!");
        }

        QuestionAnswerDataDTO answerDataDTO = new Gson().fromJson(answerData.toString(), QuestionAnswerDataDTO.class);
        if (answerDataDTO.getType() == null || answerDataDTO.getType().isEmpty()) {
            throw new IllegalArgumentException("The answer type is required!");
        }

        if (answerDataDTO.getAnswers() == null || answerDataDTO.getAnswers().isEmpty()) {
            throw new IllegalArgumentException("Question answers are required!");
        }

        QuestionTypeEnum questionTypeEnum = QuestionTypeEnum.valueOf(answerDataDTO.getType().toUpperCase());
        switch (questionTypeEnum) {
            case CHECKBOX:
                checkboxAnswerValidator.validate(answerDataDTO);
                break;
            case MATRIX:
                break;
            case POLL:
                pollAnswerValidator.validate(answerDataDTO);
                break;
            case TRIVIA:
                triviaAnswerValidator.validate(answerDataDTO);
                break;
            default:
                throw new IllegalArgumentException("Unknown question type!");
        }
    }
}
