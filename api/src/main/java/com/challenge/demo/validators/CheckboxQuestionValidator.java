package com.challenge.demo.validators;

import com.challenge.demo.dtos.QuestionAnswerDataDTO;
import org.springframework.stereotype.Component;

@Component
public class CheckboxQuestionValidator {
    void validate(QuestionAnswerDataDTO answerDataDTO) {
        int answersSize = answerDataDTO.getAnswers().size();
        if (answersSize > 10) {
            throw new IllegalArgumentException("This type of question requires at most 10 answers!");
        }
    }
}
