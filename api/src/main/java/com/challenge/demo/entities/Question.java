package com.challenge.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Document(collection = "question")
public class Question implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String questionId;

	@DBRef
	private Site site;

	@NotBlank
	@Length(min = 0, max = 250)
	private String question;

	private Object answersData;

	@CreatedDate
	private Date createdAt;

	@LastModifiedDate
	private Date updatedAt;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String  getQuestionId() {
		return questionId;
	}

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public Date getCreatedAt() {
		return createdAt;
	}

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public Object getAnswersData() {
		return answersData;
	}

	public void setAnswersData(Object answersData) {
		this.answersData = answersData;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final Question question1 = (Question) o;
		return Objects.equals(questionId, question1.questionId) &&
				Objects.equals(site, question1.site) &&
				Objects.equals(question, question1.question) &&
				Objects.equals(createdAt, question1.createdAt) &&
				Objects.equals(updatedAt, question1.updatedAt);
	}

	@Override
	public int hashCode() {
		return Objects.hash(questionId, site, question, createdAt, updatedAt);
	}

	@Override
	public String toString() {
		return "Question{" +
				"questionId='" + questionId + '\'' +
				", site=" + site +
				", question='" + question + '\'' +
				", answersData=" + answersData +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
				'}';
	}
}
