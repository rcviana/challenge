package com.challenge.demo.entities;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Document(collection = "site")
public class Site implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String siteId;

	private UUID siteUUID;

	private String url;

	@CreatedDate
	private Date createdAt;

	@LastModifiedDate
	private Date updatedAt;

	public UUID getSiteUUID() {
		return siteUUID;
	}

	public void setSiteUUID(UUID siteUUID) {
		this.siteUUID = siteUUID;
	}

	public String getSiteId() {
		return siteId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final Site site = (Site) o;
		return Objects.equals(siteId, site.siteId) &&
				Objects.equals(siteUUID, site.siteUUID) &&
				Objects.equals(url, site.url) &&
				Objects.equals(createdAt, site.createdAt) &&
				Objects.equals(updatedAt, site.updatedAt);
	}

	@Override
	public int hashCode() {
		return Objects.hash(siteId, siteUUID, url, createdAt, updatedAt);
	}

	@Override
	public String toString() {
		return "Site{" +
				"siteId='" + siteId + '\'' +
				", siteUUID=" + siteUUID +
				", url='" + url + '\'' +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
				'}';
	}
}