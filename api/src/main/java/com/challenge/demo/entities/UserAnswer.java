package com.challenge.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Document(collection = "user_answer")
public class UserAnswer implements Serializable {

    @Id
    private String userAnswerId;

    private UUID userUUID;

    private UUID siteUUID;

    private String column;

    @NotBlank
    private List<String> answers;

    @DBRef
    private Question question;

    @CreatedDate
    private Date createdAt;

    @LastModifiedDate
    private Date updatedAt;

    public String getUserAnswerId() {
        return userAnswerId;
    }

    public void setUserAnswerId(String userAnswerId) {
        this.userAnswerId = userAnswerId;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public UUID getSiteUUID() {
        return siteUUID;
    }

    public void setSiteUUID(UUID siteUUID) {
        this.siteUUID = siteUUID;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAnswer that = (UserAnswer) o;
        return Objects.equals(userAnswerId, that.userAnswerId) &&
                Objects.equals(userUUID, that.userUUID) &&
                Objects.equals(siteUUID, that.siteUUID) &&
                Objects.equals(column, that.column) &&
                Objects.equals(answers, that.answers) &&
                Objects.equals(question, that.question) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userAnswerId, userUUID, siteUUID, column, answers, question, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return "UserAnswer{" +
                "userAnswerId='" + userAnswerId + '\'' +
                ", userUUID=" + userUUID +
                ", siteUUID=" + siteUUID +
                ", column='" + column + '\'' +
                ", answers='" + answers + '\'' +
                ", question=" + question +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
