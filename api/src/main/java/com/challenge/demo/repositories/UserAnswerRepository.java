package com.challenge.demo.repositories;

import com.challenge.demo.entities.UserAnswer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface UserAnswerRepository extends MongoRepository<UserAnswer, String> {
    List<UserAnswer> findByUserUUIDAndSiteUUID(UUID userUUID, UUID siteUUID);
    void removeAllByUserUUIDAndSiteUUID(UUID userUUID, UUID siteUUID);
}
