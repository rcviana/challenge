package com.challenge.demo.repositories;

import com.challenge.demo.entities.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface QuestionRepository extends MongoRepository<Question, String> {
	List<Question> findBySiteSiteId(String siteId);
}