package com.challenge.demo.repositories;

import com.challenge.demo.entities.Site;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface SiteRepository extends MongoRepository<Site, String> {
	Site findBySiteUUID(UUID siteUUID);
}