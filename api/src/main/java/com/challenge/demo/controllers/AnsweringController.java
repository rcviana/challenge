package com.challenge.demo.controllers;

import com.challenge.demo.dtos.UserAnswerDTO;
import com.challenge.demo.dtos.UserNextQuestionDTO;
import com.challenge.demo.entities.UserAnswer;
import com.challenge.demo.repositories.QuestionRepository;
import com.challenge.demo.repositories.UserAnswerRepository;
import com.challenge.demo.services.UserAnswerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/answering")
public class AnsweringController {

    private final UserAnswerService userAnswerService;
    private final UserAnswerRepository userAnswerRepository;
    private final QuestionRepository questionRepository;

    public AnsweringController(final UserAnswerService userAnswerService, final UserAnswerRepository userAnswerRepository,
                        final QuestionRepository questionRepository) {
        this.userAnswerService = userAnswerService;
        this.userAnswerRepository = userAnswerRepository;
        this.questionRepository = questionRepository;
    }

    @GetMapping("/question")
    public ResponseEntity<UserNextQuestionDTO> getNextQuestion(@RequestBody UserNextQuestionDTO parameters) {

        UserNextQuestionDTO nextQuestionDTO = userAnswerService.getNextQuestion(
                UUID.fromString(parameters.getUserUUID()),
                UUID.fromString(parameters.getSiteUUID()));

        if (nextQuestionDTO == null) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(nextQuestionDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserAnswerDTO> storeUserAnswer(@RequestBody UserAnswerDTO incomingUserAnswer) {
        return questionRepository
                .findById(incomingUserAnswer.getQuestionId())
                .map(question -> {
                    if (!question.getSite().getSiteUUID().toString().equals(incomingUserAnswer.getSiteUUID())) {
                        return new ResponseEntity<>(new UserAnswerDTO(), HttpStatus.NOT_FOUND);
                    }

                    final UserAnswer newUserAnswer = UserAnswerDTO.createUserAnswer(incomingUserAnswer, question);
                    return new ResponseEntity<>(UserAnswerDTO.build(userAnswerRepository.save(newUserAnswer)), HttpStatus.CREATED);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
