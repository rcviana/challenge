package com.challenge.demo.controllers;

import com.challenge.demo.entities.Question;
import com.challenge.demo.dtos.QuestionDTO;
import com.challenge.demo.repositories.QuestionRepository;
import com.challenge.demo.repositories.SiteRepository;
import com.challenge.demo.validators.QuestionAnswerValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private Logger logger = LoggerFactory.getLogger(QuestionAnswerValidator.class);

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    SiteRepository siteRepository;

    @Autowired
    QuestionAnswerValidator questionAnswerValidator;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<QuestionDTO> createQuestion(@RequestBody QuestionDTO incomingQuestion) {
        return siteRepository
                .findById(incomingQuestion.getSiteId())
                .map(site -> {
                    final Question newQ = QuestionDTO.createQuestion(incomingQuestion, site);
                    return new ResponseEntity<>(QuestionDTO.build(questionRepository.save(newQ)), HttpStatus.CREATED);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping()
    public ResponseEntity<List<QuestionDTO>> getQuestions() {
        return Optional
                .ofNullable(questionRepository.findAll())
                .map(questions -> ResponseEntity.ok(QuestionDTO.build(questions)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<QuestionDTO> updateQuestion(@RequestBody Question incomingQuestion, @PathVariable(value = "id") String questionId) {

        return questionRepository
                .findById(questionId)
                .map(question -> {
                    question.setQuestion(incomingQuestion.getQuestion());
                    question.setSite(incomingQuestion.getSite());
                    return new ResponseEntity<>(QuestionDTO.build(questionRepository.save(question)), HttpStatus.OK);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<QuestionDTO> deleteQuestion(@PathVariable(value = "id") String questionId) {
        return questionRepository
                .findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    return ResponseEntity.ok(QuestionDTO.build(question));
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionDTO> getQuestionById(@PathVariable(value = "id") String questionId) {
        return questionRepository
                .findById(questionId)
                .map(question -> ResponseEntity.ok(QuestionDTO.build(question)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/{id}/answers")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<QuestionDTO> createQuestionAnswers(@PathVariable(value = "id") String questionId,
                                                             @RequestBody Object newQADto) {

        try {
            questionAnswerValidator.validate(newQADto);
        } catch (Exception e) {
            logger.error(String.format("Invalid answer information! Error: '%s'", e.getMessage()));
            return ResponseEntity.badRequest().build();
        }

        return questionRepository
                .findById(questionId)
                .map(question -> {
                    question.setAnswersData(newQADto);
                    return new ResponseEntity<>(QuestionDTO.build(questionRepository.save(question)), HttpStatus.CREATED);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}/answers")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> getQuestionAnswers(@PathVariable(value = "id") String questionId) {

        return questionRepository
                .findById(questionId)
                .map(question -> {
                    Object answerData = question.getAnswersData();
                    return new ResponseEntity<>(answerData, HttpStatus.OK);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}